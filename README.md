﻿# CodeVisualizer

Proof of concept of a web platform to help people learning programming and understanding algorithms by visualizing step-by-step the execution of code. 

## Demo
Give a try to the [demo](https://catherine-mathieu.bitbucket.io/).

*  Execute code with playback buttons and slider ('Play' button to be implement).
*  Select code fragment or algorithm from drop down list (more to come).
*  Modify code or paste custom JavaScript code and use 'Play' button to recompile.
*  Note that many JavaScript features are not supported yet (eg. Function).

![uiScreenshot](https://bitbucket.org/catherine-mathieu/codevisualizer/raw/1bdbb9b095b3a61e709a519e0317b505b5528bbe/images/UserInterfaceReadme.png)

## Installation
#### Install project

1. Follow the step to download and install [node.js](https://nodejs.org/en/download/).

2. Clone CodeVisualizer repository (or [download](https://bitbucket.org/catherine-mathieu/codevisualizer/downloads/) and extract).

3. Open a terminal in the project folder.

4. Execute the command to install dependencies:

```bash
		npm install
```

#### Run application

1. Open a terminal in the project folder

2. Execute the command:
```bash
		npm start
```

#### Run  tests

1. Run application

2. Open a new terminal in the project folder

3. Execute the command:
```bash
		npm test
```

*Note that Chrome is required to run the tests.* 

## Documentation
The approach explored in this project is based on code transformation and generation using [Abstract Syntax Tree](https://en.wikipedia.org/wiki/Abstract_syntax_tree) techniques to transform the code and display an interactive visualization of the code execution. This application is the result of a Bachelor thesis project. 

The workflow involves six main steps:

1. Get a code fragment from the user.
2. Convert the code fragment into an AST.
3. Augment the AST with inspection method calls.
4. Use augmented syntax tree to generate an augmented version of the code fragment.
5. Execute the augmented code fragment to generate execution steps data.
6. Use the execution steps data to visualize the code fragment execution.

Link to thesis document coming soon. 


## Contributing
The application is still in progress, but pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)