import {generateExecutionStates} from "./generateExecutionStates";
import {transformCode} from "./transformCode";

// `ControllerCodeAnalyzer` updates and transforms a code to generate all execution states

export class CodeAnalyzer {
  updateExecutionState = (code) => {
    const transformedCode = transformCode(code);
    console.log(transformedCode);

    this.executionStates = generateExecutionStates(transformedCode);
  };
}
