import {handleAssignmentExpressionArray as handleAssignmentExpressionArrayHandler} from "./handlers/handleAssignmentExpressionArray";
import {handleAssignmentExpressionForStatementUpdate as handleAssignmentExpressionForStatementUpdateHandler} from "./handlers/handleAssignmentExpressionForStatementUpdate";
import {handleAssignmentExpressionVariable as handleAssignmentExpressionVariableHandler} from "./handlers/handleAssignmentExpressionVariable";
import {handleForStatementTest as handleForStatementTestHandler} from "./handlers/handleForStatementTest";
import {handleIfStatementTest as handleIfStatementTestHandler} from "./handlers/handleIfStatementTest";
import {handleVariableDeclarator as handleVariableDeclaratorHandler} from "./handlers/handleVariableDeclarator";

// The following assignment variables are necessary to expose the handlers to the eval function
// noinspection JSUnusedLocalSymbols
const handleAssignmentExpressionVariable = handleAssignmentExpressionVariableHandler;
// noinspection JSUnusedLocalSymbols
const handleAssignmentExpressionArray = handleAssignmentExpressionArrayHandler;
// noinspection JSUnusedLocalSymbols
const handleForStatementTest = handleForStatementTestHandler;
// noinspection JSUnusedLocalSymbols
const handleIfStatementTest = handleIfStatementTestHandler;
// noinspection JSUnusedLocalSymbols
const handleAssignmentExpressionForStatementUpdate = handleAssignmentExpressionForStatementUpdateHandler;
// noinspection JSUnusedLocalSymbols
const handleVariableDeclarator = handleVariableDeclaratorHandler;

export const cumulativeVariables = [];
export const executionStates = [];

// `generateExecutionStates` provides all functions needed for an augmented code to generate the executions states
export const generateExecutionStates = (transformedCode) => {
  executionStates.length = 0;
  cumulativeVariables.length = 0;

  eval(transformedCode);

  return executionStates;
};

export const evalTransformedCode = (transformedCode) => {
  executionStates.length = 0;
  cumulativeVariables.length = 0;

  return eval(transformedCode);
};

// noinspection JSUnusedLocalSymbols
export const updateCumulativeVariables = (name, value, scopeId, parentScopeIds, isDeclaration) => {
  let variableChangeScopeId = scopeId;

  if (name !== undefined) {
    // A variable is involved that need be updated or added to the cumulativeVariable array.

    let variable = cumulativeVariables.find(variable =>
      variable.name === name &&
      variable.scopeId === scopeId
    );

    if (variable === undefined && isDeclaration) {
      // The variable is a new declaration and should be added to the cumulativeVariable array.
      cumulativeVariables.push({name, value, scopeId});
    } else {
      if (variable === undefined) {
        // The variable was previously declared in a previous (but still active) scope and should be added to the cumulativeVariable array.
        parentScopeIds.some(parentScopeId => {
          variable = cumulativeVariables.find(
            variable => variable.name === name && variable.scopeId === parentScopeId
          );

          variableChangeScopeId = parentScopeId;

          return variable !== undefined;
        });
        variable.value = value;
      } else {
        // The variable was declared in the current scope and its value need to be updated.
        variable.value = value;
      }
    }
  }

  cumulativeVariables.forEach(cumulativeVariable =>
    cumulativeVariable.isVisibleInScope =
      parentScopeIds.includes(cumulativeVariable.scopeId) ||
      cumulativeVariable.scopeId === scopeId);

  return {
    variables: cumulativeVariables.map(variable => ({...variable})),
    variableChangeScopeId,
  };
};

// noinspection JSUnusedLocalSymbols
export const addExecutionState = (newState) => {
  executionStates.push(newState);
};