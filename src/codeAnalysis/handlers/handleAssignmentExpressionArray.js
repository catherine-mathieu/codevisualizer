export const handleAssignmentExpressionArray = (variableData, codeRange, updateCallbacks, arrayData) => {
  const {name, value, scopeId, parentScopeIds, isDeclaration} = variableData;
  const {startIndex, endIndex} = codeRange;
  const {addExecutionState, updateCumulativeVariables} = updateCallbacks;
  const {array, index} = arrayData;

  const newArray = [...array]; // Clone the array to avoid changing the original
  newArray[index] = value;

  const {variables, variableChangeScopeId} = updateCumulativeVariables(name, newArray, scopeId, parentScopeIds, isDeclaration);
  const variableChange = {name, index, variableChangeScopeId};

  const newState = {startIndex, endIndex, variableChange, variables};
  addExecutionState(newState);

  return value;
};