export const handleAssignmentExpressionVariable = (variableData, codeRange, updateCallbacks) => {
  const {name, value, scopeId, parentScopeIds, isDeclaration} = variableData;
  const {startIndex, endIndex} = codeRange;
  const {addExecutionState, updateCumulativeVariables} = updateCallbacks;

  const {variables, variableChangeScopeId} = updateCumulativeVariables(name, value, scopeId, parentScopeIds, isDeclaration);
  const variableChange = {name, variableChangeScopeId};

  const newState = {startIndex, endIndex, variableChange, variables};
  addExecutionState(newState);

  return value;
};