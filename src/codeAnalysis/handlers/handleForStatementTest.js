export const handleForStatementTest = (variableData, codeRange, updateCallbacks) => {
  const {value, scopeId, parentScopeIds, isDeclaration} = variableData;
  const {startIndex, endIndex} = codeRange;
  const {addExecutionState, updateCumulativeVariables} = updateCallbacks;

  const {variables} = updateCumulativeVariables(undefined, undefined, scopeId, parentScopeIds, isDeclaration);

  const newState = {startIndex, endIndex, variables};
  addExecutionState(newState);

  return value;
};