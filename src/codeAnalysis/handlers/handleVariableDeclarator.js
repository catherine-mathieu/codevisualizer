export const handleVariableDeclarator = (variableData, codeRange, updateCallbacks) => {
  const {name, value, scopeId, parentScopeIds, isDeclaration} = variableData;
  const {startIndex, endIndex} = codeRange;
  const {addExecutionState, updateCumulativeVariables} = updateCallbacks;

  const newValue = Array.isArray(value) ? [...value] : value;
  const {variables, variableChangeScopeId} = updateCumulativeVariables(name, newValue, scopeId, parentScopeIds, isDeclaration);
  const variableChange = {name, variableChangeScopeId};

  const newState = {startIndex, endIndex, variableChange, variables};
  addExecutionState(newState);

  return value;
};