import {transformCodeAndGenerateExecutionStates} from "../transformCodeAndGenerateExecutionStates";

describe("Assignment statement tests", () => {
  test("Array assignment statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let items = [8, 5, 4, 6, 2, 1, 7, 9, 3];

items = [8, 5, 4, 6];
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[1]).toMatchObject({startIndex: 42, endIndex: 62});
  });

  test("Variable assignment statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let a = 0;

a = a + 1;
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[1]).toMatchObject({startIndex: 12, endIndex: 21});
  });

  test("For loop update assignment statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let a = 0;

for (let i = 0; i < 10; i = i + 1) {
  a = a + 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[4]).toMatchObject({startIndex: 36, endIndex: 45});
  });

  test("Multiple assignment statements produces the correct number of execution states.", () => {
    const initialCode = `
let value = false;
value = null;
value = "This is a string.";
value = 10;
value = Infinity;
value = NaN;
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(6);
  });
});