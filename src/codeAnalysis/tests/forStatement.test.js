import {transformCodeAndGenerateExecutionStates} from "../transformCodeAndGenerateExecutionStates";

describe("For statement tests", () => {
  test("For statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let a = 0;

for (let i = 0; i < 10; i = i + 1) {
  a = a + 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[2]).toMatchObject({startIndex: 28, endIndex: 34});
  });

  test("For statement produces execution states with correct number of execution states.", () => {
    const initialCode = `
let a = 0;

for (let i = 0; i < 10; i = i + 1) {
  a = a + 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(33);
  });

  test("For statement produces few execution states if condition is not met.", () => {
    const initialCode = `
let a = 0;

for (let i = 11; i < 10; i = i + 1) {
  a = a + 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(3);
  });
});