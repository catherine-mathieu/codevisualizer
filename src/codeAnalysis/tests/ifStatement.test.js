import {transformCodeAndGenerateExecutionStates} from "../transformCodeAndGenerateExecutionStates";

describe("If statement tests", () => {
  test("If statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let a = 0;

if (a < 10) {
  a = 5;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[1]).toMatchObject({startIndex: 16, endIndex: 22});
  });

  test("Else If statement produces execution states with correct highlight range.", () => {
    const initialCode = `
let a = 0;

if (a > 10) {
  a = 5;
}
else if(a < 10){
  a = 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[2]).toMatchObject({startIndex: 45, endIndex: 51});
  });

  test("If statement with positive result produces the correct number of execution states.", () => {
    const initialCode = `
let a = 0;

if (a < 10) {
  a = 5;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(3);
  });

  test("If statement with negative result produces the correct number of execution states.", () => {
    const initialCode = `
let a = 11;

if (a < 10) {
  a = 5;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(2);
  });

  test("Else If statement produces the correct number of execution states.", () => {
    const initialCode = `
let a = 0;

if (a > 10) {
  a = 5;
}
else if(a < 10){
  a = 1;
}`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates.length).toBe(4);
  });
});