import {transformCodeAndGenerateExecutionStates} from "../transformCodeAndGenerateExecutionStates";

describe("Variable declaration tests", () => {
  test("Variable declaration and assignation with const statement produces execution states with correct highlight range.", () => {
    const initialCode = "const value = 1;";
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0]).toMatchObject({startIndex: 6, endIndex: 15});
  });

  test("Variable declaration with let statement produces execution states with correct highlight range.", () => {
    const initialCode = "let value;";
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0].startIndex).toBe(4);
    expect(executionStates[0].endIndex).toBe(9);
  });

  test("Variable declaration from a codeFragment example produces execution states with correct highlight range.", () => {
    const initialCode = `
const items = [8, 5, 4, 6, 2, 1, 7, 9, 3];
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0].startIndex).toBe(6);
    expect(executionStates[0].endIndex).toBe(41);
  });

  test("Variable declaration and assignation with const statement produces execution states with correct variables data.", () => {
    const initialCode = "const value = 3;";
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0].variableChange.name).toBe("value");
    expect(executionStates[0].variables.length).toBe(1);
    expect(executionStates[0].variables[0].name).toBe("value");
    expect(executionStates[0].variables[0].value).toBe(3);
  });

  test("Variable declaration and assignation with multiple const statement produces execution states with correct variables data.", () => {
    const initialCode = `
const value = 3;
const value2 = 5;
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0].variableChange.name).toBe("value");
    expect(executionStates[0].variables.length).toBe(1);
    expect(executionStates[0].variables[0].name).toBe("value");
    expect(executionStates[0].variables[0].value).toBe(3);

    expect(executionStates[1].variableChange.name).toBe("value2");
    expect(executionStates[1].variables.length).toBe(2);
    expect(executionStates[1].variables[0].name).toBe("value");
    expect(executionStates[1].variables[0].value).toBe(3);

    expect(executionStates[1].variables[1].name).toBe("value2");
    expect(executionStates[1].variables[1].value).toBe(5);
  });

  test("Variable declaration and assignation with multiple const statement on a single line produces execution states with correct variables data.", () => {
    const initialCode = "const value = 3, value2 = 5;";
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[0].variableChange.name).toBe("value");
    expect(executionStates[0].variables.length).toBe(1);
    expect(executionStates[0].variables[0].name).toBe("value");
    expect(executionStates[0].variables[0].value).toBe(3);

    expect(executionStates[1].variableChange.name).toBe("value2");
    expect(executionStates[1].variables.length).toBe(2);
    expect(executionStates[1].variables[0].name).toBe("value");
    expect(executionStates[1].variables[0].value).toBe(3);

    expect(executionStates[1].variables[1].name).toBe("value2");
    expect(executionStates[1].variables[1].value).toBe(5);
  });

  test("Variable declaration and assignation when out of scope produces execution states with correct variables visibility.", () => {
    const initialCode = `
const a = "a";

{ 
  const b = "b";
}

{
  const e = "e";
}
`;
    const executionStates = transformCodeAndGenerateExecutionStates(initialCode);

    expect(executionStates[2].variables[0]).toMatchObject({isVisibleInScope: true, name: "a"});
    expect(executionStates[2].variables[1]).toMatchObject({isVisibleInScope: false, name: "b"});
  });
});