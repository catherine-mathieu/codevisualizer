import generate from "@babel/generator";
import {parse} from "@babel/parser";
import traverse from "@babel/traverse";
import {transformAssignmentExpression} from "./transformers/transformAssignmentExpression";
import {transformForStatement} from "./transformers/transformForStatement";
import {transformIfStatement} from "./transformers/transformIfStatement";
import {transformVariableDeclarator} from "./transformers/transformVariableDeclarator";

// `transformCode` transforms a code using AST into a new augmented code.
// In the augmented code, all steps are wrapped in function calls which provides data for visualization
//   - startIndex and endIndex to highlight the code
//   - Variable names and values

export const transformCode = (code) => {
  // parse the code to ast
  const ast = parse(code);

  // transform the ast
  // noinspection JSCheckFunctionSignatures
  traverse(ast, {
    VariableDeclarator: transformVariableDeclarator,
    ForStatement: transformForStatement,
    IfStatement: transformIfStatement,
    AssignmentExpression: transformAssignmentExpression,
  });

  // generate code from ast
  const output = generate(ast, code);

  return output.code;
};