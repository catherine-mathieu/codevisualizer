import {generateExecutionStates} from "./generateExecutionStates";
import {transformCode} from "./transformCode";

export const transformCodeAndGenerateExecutionStates = (code) => {
  const transformedCode = transformCode(code.trim());

  return generateExecutionStates(transformedCode);
};