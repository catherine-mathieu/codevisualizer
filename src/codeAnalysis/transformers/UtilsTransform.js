import {
  arrayExpression,
  booleanLiteral,
  identifier,
  numericLiteral,
  objectExpression,
  objectProperty,
  stringLiteral
} from "@babel/types";

export class UtilsTransform {
  // todo: Add suffix node for the next three functions
  static  getVariableData = (name, value, scopeId, parentScopeIds, isDeclaration) => objectExpression([
    objectProperty(identifier("name"), stringLiteral(name)),
    objectProperty(identifier("value"), value),
    objectProperty(identifier("scopeId"), numericLiteral(scopeId)),
    objectProperty(identifier("parentScopeIds"), arrayExpression(parentScopeIds)),
    objectProperty(identifier("isDeclaration"), booleanLiteral(isDeclaration)),
  ]);

  static getCodeRange = (path) => objectExpression([
    objectProperty(identifier("startIndex"), numericLiteral(path.node.start)),
    objectProperty(identifier("endIndex"), numericLiteral(path.node.end)),
  ]);

  static getUpdateCallbacks = () => objectExpression([
    objectProperty(identifier("addExecutionState"), identifier("addExecutionState")),
    objectProperty(identifier("updateCumulativeVariables"), identifier("updateCumulativeVariables")),
  ]);

  static getParentScopeIds = (path) => {
    const parentScopeIds = [];
    let parentScope = path.scope.parent;

    while (parentScope != undefined) {
      parentScopeIds.push(parentScope.uid);
      parentScope = parentScope.parent;
    }

    return parentScopeIds;
  };
}