import {callExpression, identifier, isIdentifier, numericLiteral, objectExpression, objectProperty} from "@babel/types";
import {UtilsTransform} from "./UtilsTransform";

export const transformAssignmentExpression = function (path) {
  const parentScopeIds = UtilsTransform.getParentScopeIds(path);
  const parentScopeIdNumericLiterals =
    parentScopeIds.map(parentScopeId => numericLiteral(parentScopeId));

  const isDeclaration = false;

  if (path.parentPath.isExpressionStatement() && (isIdentifier(path.node.left.object))) {
    transformAssignmentExpressionArray(path, parentScopeIdNumericLiterals, isDeclaration);
  } else if (path.parentPath.isExpressionStatement() && (isIdentifier(path.node.left))) {
    transformAssignmentExpressionVariable(path, parentScopeIdNumericLiterals, isDeclaration);
  } else if (path.parentPath.isForStatement()) {
    transformAssignmentExpressionUpdateForStatement(path, parentScopeIdNumericLiterals, isDeclaration);
  }
};

const transformAssignmentExpressionArray = (path, parentScopeIdNumericLiterals, isDeclaration) => {
  const variableData = UtilsTransform.getVariableData(
    path.node.left.object.name,
    path.node.right,
    path.scope.uid,
    parentScopeIdNumericLiterals,
    isDeclaration,
  );
  const codeRange = UtilsTransform.getCodeRange(path);
  const updateCallbacks = UtilsTransform.getUpdateCallbacks();
  const arrayData = objectExpression([
    objectProperty(identifier("array"), path.node.left.object),
    objectProperty(identifier("index"), path.node.left.property),
  ]);

  path.node.right = callExpression(
    identifier("handleAssignmentExpressionArray"),
    [variableData, codeRange, updateCallbacks, arrayData]
  );
};

const transformAssignmentExpressionVariable = (path, parentScopeIdNumericLiterals, isDeclaration) => {
  const variableData = UtilsTransform.getVariableData(
    path.node.left.name,
    path.node.right,
    path.scope.uid,
    parentScopeIdNumericLiterals,
    isDeclaration
  );
  const codeRange = UtilsTransform.getCodeRange(path);
  const updateCallbacks = UtilsTransform.getUpdateCallbacks();

  path.node.right = callExpression(
    identifier("handleAssignmentExpressionVariable"),
    [variableData, codeRange, updateCallbacks]
  );
};

const transformAssignmentExpressionUpdateForStatement = (path, parentScopeIdNumericLiterals, isDeclaration) => {
  const variableData = UtilsTransform.getVariableData(
    path.node.left.name,
    path.node.right,
    path.scope.uid,
    parentScopeIdNumericLiterals,
    isDeclaration
  );

  const codeRange = UtilsTransform.getCodeRange(path);
  const updateCallbacks = UtilsTransform.getUpdateCallbacks();

  path.node.right = callExpression(
    identifier("handleAssignmentExpressionForStatementUpdate"),
    [variableData, codeRange, updateCallbacks]
  );
};