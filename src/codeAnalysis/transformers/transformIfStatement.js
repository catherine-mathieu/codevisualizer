import {callExpression, identifier, numericLiteral, objectExpression, objectProperty} from "@babel/types";
import {UtilsTransform} from "./UtilsTransform";

export const transformIfStatement = function (path) {
  const parentScopeIds = UtilsTransform.getParentScopeIds(path);
  const parentScopeIdNumericLiterals =
    parentScopeIds.map(parentScopeId => numericLiteral(parentScopeId));

  const isDeclaration = false;

  const variableData = UtilsTransform.getVariableData(
    "",
    path.node.test,
    path.scope.uid,
    parentScopeIdNumericLiterals,
    isDeclaration,
  );
  const codeRange = objectExpression([
    objectProperty(identifier("startIndex"), numericLiteral(path.node.test.start)),
    objectProperty(identifier("endIndex"), numericLiteral(path.node.test.end)),
  ]);
  const updateCallbacks = UtilsTransform.getUpdateCallbacks();

  path.node.test = callExpression(
    identifier("handleIfStatementTest"),
    [variableData, codeRange, updateCallbacks]
  );
};