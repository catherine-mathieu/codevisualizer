import {callExpression, identifier, numericLiteral} from "@babel/types";
import {UtilsTransform} from "./UtilsTransform";

export const transformVariableDeclarator = (path) => {
  const variableValue = path.node.init
    ? path.node.init
    : identifier("undefined");

  const parentScopeIds = UtilsTransform.getParentScopeIds(path);
  const parentScopeIdNumericLiterals =
    parentScopeIds.map(parentScopeId => numericLiteral(parentScopeId));

  const isDeclaration = true;

  const variableData = UtilsTransform.getVariableData(
    path.node.id.name,
    variableValue,
    path.scope.uid,
    parentScopeIdNumericLiterals,
    isDeclaration,
  );
  const codeRange = UtilsTransform.getCodeRange(path);
  const updateCallbacks = UtilsTransform.getUpdateCallbacks();

  path.node.init = callExpression(
    identifier("handleVariableDeclarator"),
    [variableData, codeRange, updateCallbacks]
  );
};