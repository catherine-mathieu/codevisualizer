export const algorithmBubbleSort = {
  name: "Bubble Sort Algorithm",
  testReturnValue: "items;",
  code: `
const items = [8, 5, 4, 6, 2, 1, 7, 9, 3];

// Algorithm
for (let i = 0; i < items.length; i = i + 1) {
  for (let j = items.length; j >= i; j = j - 1) {
    if (items[j] < items[j - 1]) {
      const swap = items[j];
      items[j] = items[j - 1];
      items[j - 1] = swap;
    }
  }
}
`,
};