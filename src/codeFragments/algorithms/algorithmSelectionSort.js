export const algorithmSelectionSort = {
  name: "Selection Sort Algorithm",
  testReturnValue: "items;",
  code: `
const items = [8, 5, 4, 6, 2, 1, 7, 9, 3];

// Algorithm
for (let i = 0; i < items.length; i = i + 1) {
  let minimum = i;
  for (let j = i + 1; j < items.length; j = j + 1) {
    if (items[minimum] > items[j]) {
      minimum = j;
    }
  }
  if (minimum !== i) {
    const swap = items[i];
    items[i] = items[minimum];
    items[minimum] = swap;
  }
}
`,
};