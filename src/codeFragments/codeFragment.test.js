import {evalTransformedCode} from "../codeAnalysis/generateExecutionStates";
import {transformCode} from "../codeAnalysis/transformCode";
import {codeFragmentLibrary} from "./codeFragmentLibrary";

describe("Code Fragment tests", () => {
  const testTransformedCodeResult = (initialCode, returnValueCode) => {
    const transformedCode = transformCode(initialCode);

    const returnValueInitial = eval(initialCode + "\n" + returnValueCode);
    const returnValueTransformed = evalTransformedCode(transformedCode + "\n" + returnValueCode);

    return {returnValueInitial, returnValueTransformed};
  };

  codeFragmentLibrary.forEach(codeFragment => {
    test("The " + codeFragment.name + " produces correct output values.", () => {

      const {returnValueInitial, returnValueTransformed} =
        testTransformedCodeResult(codeFragment.code, codeFragment.testReturnValue);

      expect(returnValueInitial).toStrictEqual(returnValueTransformed);
    });
  });
});