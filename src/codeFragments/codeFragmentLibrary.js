import {featureCollectionSet} from "./features/featureCollectionSet";
import {algorithmBubbleSort} from "./algorithms/algorithmBubbleSort";
import {featureClosure} from "./features/featureClosure";
import {featureComment} from "./features/featureComment";
import {featureFunctionArrow} from "./features/featureFunctionArrow";
import {featureFunctionExpression} from "./features/featureFunctionExpression";
import {featureIf} from "./features/featureIf";
import {featureForLoop} from "./features/featureForLoop";
import {featureFunctionDeclaration} from "./features/featureFunctionDeclaration";
import {featureHiding} from "./features/featureHiding";
import {featureIfElse} from "./features/featureIfElse";
import {featureIfElseElse} from "./features/featureIfElseElse";
import {featureWhileLoop} from "./features/featureWhileLoop";
import {featureObject} from "./features/featureObject";
import {featureOperator} from "./features/featureOperator";
import {featureCollectionArray} from "./features/featureCollectionArray";
import {featurePrimitiveType} from "./features/featurePrimitiveType";
import {featureRecursion} from "./features/featureRecursion";
import {featureScope} from "./features/featureScope";
import {algorithmSelectionSort} from "./algorithms/algorithmSelectionSort";

export const codeFragmentLibrary = [
  algorithmBubbleSort,
  algorithmSelectionSort,
  featureClosure,
  featureCollectionArray,
  featureCollectionSet,
  featureComment,
  featureForLoop,
  featureFunctionArrow,
  featureFunctionDeclaration,
  featureFunctionExpression,
  featureHiding,
  featureIf,
  featureIfElse,
  featureIfElseElse,
  featureObject,
  featureOperator,
  featurePrimitiveType,
  featureRecursion,
  featureScope,
  featureWhileLoop,
];