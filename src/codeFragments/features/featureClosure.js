export const featureClosure = {
  name: "Closure Feature",
  testReturnValue: "f2;",
  code: `
const f = () => {
let a = 1;
let b = 2;

return {
getA: () => a,
setA: (value) => a = value,
};
}

const f1 = f();
const f2 = f();

f2.setA(3);
`,
};
