export const featureCollectionArray = {
  name: "Array Collection Feature",
  testReturnValue: "integerArray;",
  code: `
const integerArray = [2, 4, 6, 8];

const floatArray = [2.2, 4.4, 6.6, 8.8];

const charArray = ["b", "d", "f","h"];

const stringArray = ["banana", "apple", "pineapple","grenada"];
`,
};
