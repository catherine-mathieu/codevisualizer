export const featureCollectionSet = {
  name: "Collection Set Feature",
  testReturnValue: "setCollection;",
  code: `
let setCollection = new Set();

setCollection.add(1);
setCollection.add(2);

let food = {fruit: "banana", vegetable: "carrot"};
setCollection.add(food);
setCollection.add("meat");

setCollection.size;
setCollection.delete("meat");
`,
};

