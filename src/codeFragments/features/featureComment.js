export const featureComment = {
  name: "Comment Feature",
  testReturnValue: "b;",
  code: `
const a = 1;
//This is a comment!

const b = 2;
//This is another comment!
`,
};