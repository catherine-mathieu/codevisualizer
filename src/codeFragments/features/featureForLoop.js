export const featureForLoop = {
  name: "For Loop Feature",
  testReturnValue: "a;",
  code: `
let a = 0;

for (let i = 0; i < 10; i = i + 1) {
  a = a + 1;
}
`,
};