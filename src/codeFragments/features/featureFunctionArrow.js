export const featureFunctionArrow = {
  name: "Function Arrow Feature",
  testReturnValue: "halfValue;",
  code: `
const splitInHalf = (number) => {
  return number / 2;
};

const halfValue = splitInHalf(8);
`,
};