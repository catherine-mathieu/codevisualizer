export const featureFunctionDeclaration = {
  name: "Function Declaration Feature",
  testReturnValue: "a;",
  code: `
function splitInHalf(number) {
  return number / 2;
}

let a = splitInHalf(8);
`,
};