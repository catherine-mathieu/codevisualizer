export const featureFunctionExpression = {
  name: "Function Expression Feature",
  testReturnValue: "halfValue;",
  code: `
const splitInHalf = function(number) {
  return number / 2;
}

const halfValue = splitInHalf(8);
`,
};