export const featureHiding = {
  name: "Hiding Feature",
  testReturnValue: "variableHided;",
  code: `
const a = 1;

if (true) {
  const a = 2;
}
const variableHided = (a === 2);
`,
};