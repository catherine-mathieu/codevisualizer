export const featureIf = {
  name: "If Feature",
  testReturnValue: "a;",
  code: `
let a = 0;

if (a < 10) {
  a = 5;
}
`,
};