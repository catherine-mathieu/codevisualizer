export const featureIfElse = {
  name: "If Else Feature",
  testReturnValue: "a;",
  code: `
let a = 0;

if (a > 10) {
  a = 5;
}
else if(a < 10){
  a = 1;
}
`,
};