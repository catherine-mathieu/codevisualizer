export const featureIfElseElse = {
  name: "If Else & Else Feature",
  testReturnValue: "a;",
  code: `
let a = 0;

if (a > 10) {
  a = 5;
}
else if(a > 5){
  a = 1;
}
else{
  a = 2
}
`,
};