export const featureObject = {
  name: "Object Feature",
  testReturnValue: "obj;",
  code: `
const obj = {
  name: "Hello",
  items: [1, 2, 3],
  props: {
    age: 32,
  },
  isValid: true,
  moreProps: {
    grade: "A",
  },
  identity: x => x,
  color: undefined,
};

const a = 1;
obj.moreProps.a = a;
obj.moreProps.self = obj;
obj.moreProps.other = obj.props;
`,
};