export const featureOperator = {
  name: "Operator Feature",
  testReturnValue: "j;",
  code: `
const a = 1;
const b = 2;

let c = a;
let d = (a < b);
let e = c && (d === a);
let f = c + c;
let g = f * 9;
let h = g / 2;
let i = h % 4;
let j = (a + f) * 2 / 3;
`,
};