export const featurePrimitiveType = {
  name: "Primitive Type Feature",
  testReturnValue: "numberValue;",
  code: `
const booleanValue = false;
const nullValue = null;
const text = "This is a string.";
const numberValue = 10;
const infinityValue = Infinity;
const notANumberValue = NaN;
let undefinedValue;
`,
};











