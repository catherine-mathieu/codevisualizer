export const featureRecursion = {
  name: "Recursion Feature",
  testReturnValue: "factorialValue;",
  code: `
const factorial = (n) => {
  if (n < 0) {
    return;
  }

  if (n === 0) {
    return 1;
  }

  return n * factorial(n - 1);
}

const factorialValue = factorial(3);
`,
};