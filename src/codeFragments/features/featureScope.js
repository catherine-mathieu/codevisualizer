// export const featureScope = {
//   name: "Scope Feature",
//   code: `
// let initialName = "firstName";
//
// function rename(){
//   initialName = "secondName";
// }
//
// const accessName = (initialName === "secondName");
// `,
// };

export const featureScope = {
  name: "Scope Feature",
  testReturnValue: "a;",
  code: `
// scope 0;
const a = "a";

{ // scope 1, parent 0
  const b = "b";

  { // scope 2, parents 1, 0
    const c = "c";
    
    { // scope 3, parents 2, 1, 0
        const d = "d";
    }
  }
}

{ // scope 4, parent 0
  const e = "e";
}
`,
};