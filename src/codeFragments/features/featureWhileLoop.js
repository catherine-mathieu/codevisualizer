export const featureWhileLoop = {
  name: "While Loop Feature",
  testReturnValue: "result;",
  code: `
let result = 0;

while (result < 10) {
  result = result + 1;
}
`,
};

