import IconButton from "@material-ui/core/IconButton";
import React from "react";
import {Tooltip} from "../Util/Tooltip.js";

export const ButtonIcon = (props) => {
  const Icon = props.icon;

  return (
    <Tooltip title={props.title} closer>
      <IconButton data-id={props["data-id"]} color="primary" aria-label={props.title} onClick={props.onClick}>
        <Icon color="primary" fontSize={props.fontSize}/>
      </IconButton>
    </Tooltip>
  );
};