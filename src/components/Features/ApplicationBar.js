import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import EditIcon from "@material-ui/icons/Edit";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import React from "react";
import {ButtonIcon} from "../Common/ButtonIcon";
import {ApplicationDescription} from "./ApplicationDescription";
import {ApplicationIcon} from "./ApplicationIcon";
import {ApplicationName} from "./ApplicationName";

const useStyles = makeStyles(theme => ({
  bar: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(0),
    backgroundColor: "white",
    boxShadow: "inset 0 -1px 0 rgba(96, 128, 140, .125)",
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(1),
  },
  description: {
    flexGrow: 1,

  },
}));

const ApplicationBar = () => {
  const classes = useStyles();

  return (
    <Toolbar variant="dense" className={classes.bar}>
      <ApplicationIcon className={classes.icon}/>
      <ApplicationName className={classes.title} variant="h6" />
      <ApplicationDescription variant="overline" color="textSecondary" className={classes.description} />
      <ButtonIcon title={"Settings"} icon={EditIcon} />
      <ButtonIcon title={"Help"} icon={HelpOutlineIcon} />
    </Toolbar>
  );
};

export {ApplicationBar};
