import Typography from "@material-ui/core/Typography";
import React from "react";

const ApplicationDescription = (props) => {
  const descriptionText = "Learn programming, algorithms and data structures step by step";

  return (
    <Typography {...props}>
      {descriptionText}
    </Typography>
  );
};

export {ApplicationDescription};
