import React from "react";

const ApplicationIcon = () => {
  return (
    <img src="icon.png" alt="Application icon" style={{height: "32px", width: "32px"}} />
  );
};

export {ApplicationIcon};