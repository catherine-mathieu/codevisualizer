import {makeStyles} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";

const useStyles = makeStyles(() => ({
  titleSecondary: {
    fontWeight: "normal",
  },
}));

const ApplicationName = (props) => {
  const classes = useStyles();

  return (
    <span className={props.className}>
      <Typography variant={props.variant} display="inline">
        {"Code"}
      </Typography>
      <Typography id="applicationTitle2" variant={props.variant} display="inline" className={classes.titleSecondary}>
        {"Visualizer"}
      </Typography>
    </span>
  );
};

export {ApplicationName};