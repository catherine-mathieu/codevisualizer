import {createMuiTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@material-ui/styles";
import React from "react";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#1976d2",
    },
    secondary: {
      main: "#DC6E4E",
    },
    action : {
      active: "#1976d2",
    },
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
});

const ApplicationTheme = (props) => {
  return (
    <ThemeProvider theme={theme}>
      {props.children}
    </ThemeProvider>
  );
};

export {ApplicationTheme};