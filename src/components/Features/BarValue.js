import React from "react";

// `BarValue` displays a single bar to represent a quantity with a custom height

export const BarValue = (props) => {
  const className =
    "bar " +
    (props.highlight ? "barHighlight" : "");

  const style = {
    height: `${props.value * 20}px`
  };

  return (
    <div className={className} style={style}>
      {props.value}
    </div>
  );
};