import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-dreamweaver";
import {makeStyles} from "@material-ui/core/styles";
import React, {useState} from "react";
import AceEditor from "react-ace";
import {ContentPane} from "./ContentPane";

const useStyles = makeStyles(() => ({
  codeEditorContentRaw: {
    display: "none",
  },
}));

// `CodeEditor` displays an Ace code editor with highlightable range and a run button
export const CodeEditor = (props) => {
  const classes = useStyles();
  const [editor, setEditor] = useState(undefined);
  const markers = getMarkers(editor, props.startIndex, props.endIndex);

  const onEditorLoad = (newEditor) => {
    setEditor(newEditor);
  };

  const onCodeChange = () => {
    props.onCodeChange(editor.getValue());
  };

  const aceEditorProps = {
    onLoad: onEditorLoad,
    mode: "javascript",
    theme: "dreamweaver",
    value: props.code,
    markers,
    onChange: onCodeChange,
    name: "aceCodeEditor",
    width: "",
    height: "",
  };

  return (
    <ContentPane title={"codeEditor"} width="33%">
      <div data-id="codeEditorContentRaw" className={classes.codeEditorContentRaw}>
        {props.code}
      </div>
      <AceEditor {...aceEditorProps}/>
    </ContentPane>
  );
};

const getMarkers = (editor, startIndex, endIndex) => {
  if (editor !== undefined && startIndex !== undefined && endIndex !== undefined) {
    const startPosition = editor.session.doc.indexToPosition(startIndex, 0);
    const endPosition = editor.session.doc.indexToPosition(endIndex, 0);

    return [{
      startRow: startPosition.row,
      startCol: startPosition.column,
      endRow: endPosition.row,
      endCol: endPosition.column,
      className: "line-highlight",
      type: "line"
    }];
  }
};