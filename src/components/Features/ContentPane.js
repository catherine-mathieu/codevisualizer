import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core/styles";
import React from "react";
import {Tooltip} from "../Util/Tooltip.js";

const useStyles = makeStyles(theme => ({
  box: {
    boxShadow: "inset 0 -1px 0 rgba(96, 128, 140, .125), inset 1px 0px 0px rgba(96, 128, 140, .125)",
    color: theme.palette.text.secondary,
    overflow: "hidden",
    minHeight: "400px",
  },

  tooltip: {
    minHeight: "400px",
  },
}));

export const ContentPane = (props) => {
  const classes = useStyles();

  return (
    <Tooltip title={props.title}>
      <Box width={"100%"} className={classes.box}>
        {props.children}
      </Box>
    </Tooltip>
  );
};