import React from "react";
import {ContentPane} from "./ContentPane";
import {VariableBox} from "./VariableBox";

// `DataVisualization` displays a representation of all variables

export const DataVariable = (props) => {
  const variables = props.variables
    .map((variable, index) => {
      const variableBoxProps = {
        "data-id": `variable-${index}-${variable.name}`,
        key: `${variable.scopeId}-${variable.name}`,
        ...variable,
        highlight: variable.name === props.variableChange?.name && variable.scopeId === props.variableChange.variableChangeScopeId,
        isVisibleInScope: variable.isVisibleInScope,
      };

      return <VariableBox {...variableBoxProps}/>
    });

  return (
    <ContentPane title={"dataVariable"} width="33%">
      {variables}
    </ContentPane>
  );
};