import React from "react";
import {ContentPane} from "./ContentPane";
import {DiagramBar} from "./DiagramBar";

// `DataVisualization` displays a representation of all variables

export const DataVisualization = (props) => {
  const variableArrays = props.variables
    .filter(variable => Array.isArray(variable.value))
    .map(variable => {
      const diagramBarProps = {
        key: `${variable.scopeId}-${variable.name}`,
        values: variable.value,
        highlight: variable.name === props.variableChange?.name,
        highlightIndex: props.variableChange?.index,
      };

      return <DiagramBar {...diagramBarProps}/>
    });

  return (
    <ContentPane title={"dataVariable"} width="33%">
      {variableArrays}
    </ContentPane>
  );
};