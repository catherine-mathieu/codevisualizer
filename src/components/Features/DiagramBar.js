import React from "react";
import {BarValue} from "./BarValue";

// `DiagramBar` displays a bar diagram to represent an array

export const DiagramBar = (props) => {
  const items = props.values.map((value, index) => {
    const highlightAll = props.highlight && props.highlightIndex === undefined;
    const highlightSingle = props.highlight && props.highlightIndex === index;

    return <BarValue key={index} value={value} highlight={highlightAll || highlightSingle}/>;
  });

  return (
    <div className="DiagramBar">
      {items}
    </div>
  );
};