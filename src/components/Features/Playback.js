import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import React from "react";
import {PlaybackControls} from "./PlaybackControls";
import {PlaybackLibrary} from "./PlaybackLibrary";
import {PlaybackSlider} from "./PlaybackSlider";
import {PlaybackStepCounter} from "./PlaybackStepCounter";

const useStyles = makeStyles(theme => ({
  bar: {
    boxShadow: "inset 0 -1px 0 rgba(96, 128, 140, .125)",
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
}));

export const Playback = (props) => {
  const classes = useStyles();

  //todo: Split props for each components
  const playbackSliderProps = {
    stepIndex: props.stepIndex,
    stepCount: props.stepCount,
    onChange: props.onChange,
    onCodeBuildClick: props.onCodeBuildClick,
  };

  return (
    <Toolbar variant="dense" className={classes.bar}>
      <PlaybackLibrary
        codeFragmentNames={props.codeFragmentNames}
        onCodeFragmentSelect={props.onCodeFragmentSelect}
      />
      <PlaybackControls {...playbackSliderProps}/>
      <PlaybackSlider {...playbackSliderProps}/>
      <PlaybackStepCounter {...playbackSliderProps}/>
    </Toolbar>
  );
};