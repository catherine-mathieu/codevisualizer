import {makeStyles} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import PlayCircleFilledWhiteIcon from "@material-ui/icons/PlayCircleFilledWhite";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import React from "react";
import {ButtonIcon} from "../Common/ButtonIcon";

const useStyles = makeStyles(() => ({
  toolbar: {
    minHeight: 24,
  },
  divider: {
    height: 24,
  },
}));


export const PlaybackControls = (props) => {
  const classes = useStyles();

  const onPlayClick = () => {
    props.onCodeBuildClick();
  };

  const onFirstClick = () => {
    props.onChange(0);
  };

  const onPreviousClick = () => {
    props.onChange(props.stepIndex - 1);
  };

  const onNextClick = () => {
    props.onChange(props.stepIndex + 1);
  };

  const onLastClick = () => {
    props.onChange(props.stepCount - 1);
  };

  return (
    <Toolbar className={classes.toolbar}>
      <ButtonIcon
        data-id="button-playbackControlFirstStep"
        title="First step"
        icon={FirstPageIcon}
        onClick={onFirstClick}
      />
      <ButtonIcon
        data-id="button-playbackControlPrevious"
        title="Previous step" icon={SkipPreviousIcon}
        onClick={onPreviousClick}
      />
      <ButtonIcon
        data-id="button-playbackControlPlay"
        title="Play"
        icon={PlayCircleFilledWhiteIcon}
        onClick={onPlayClick}
      />
      <ButtonIcon
        data-id="button-playbackControlNext"
        title="Next step"
        icon={SkipNextIcon}
        onClick={onNextClick}
      />
      <ButtonIcon
        data-id="button-playbackControlLastStep"
        title="Last step"
        icon={LastPageIcon}
        onClick={onLastClick}
      />
    </Toolbar>
  );
};