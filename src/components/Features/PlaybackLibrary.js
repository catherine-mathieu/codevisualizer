import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import {makeStyles} from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
}));

export const PlaybackLibrary = (props) => {
  const classes = useStyles();
  const [codeFragment, setCodeFragment] = React.useState(0);

  const codeFragmentMenuItems = props.codeFragmentNames
    .map((codeFragmentName, index) =>
      <MenuItem data-id={"MenuItem-" + codeFragmentName} value={index} key={codeFragmentName}>
        {codeFragmentName}
      </MenuItem>
    );

  const handleChange = (event) => {
    setCodeFragment(event.target.value);
    props.onCodeFragmentSelect(event.target.value);
  };

  return (
    <div className="PlaybackLibrary">
      <FormControl className={classes.formControl}>
        <Select value={codeFragment} onChange={handleChange}>
          {codeFragmentMenuItems}
        </Select>
      </FormControl>
    </div>
  );
};