import Slider from "@material-ui/core/Slider";
import {makeStyles} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import React from "react";

// `Playback` displays a slider and the current step

const useStyles = makeStyles(() => ({
  progress: {
    flexGrow: 1,
    width: "auto",
  },
}));

export const PlaybackSlider = (props) => {
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    props.onChange(newValue);
  };

  return (
    <Tooltip title="Step progression">
      <Slider
        data-id="slider-playbackControlStepProgression"
        value={props.stepIndex}
        onChange={handleChange}
        aria-labelledby="continuous-slider"
        className={classes.progress}
        max={props.stepCount - 1}
      />
    </Tooltip>
  );
};

