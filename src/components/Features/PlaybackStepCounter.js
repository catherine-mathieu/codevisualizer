import {makeStyles} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import React from "react";

const useStyles = makeStyles(() => ({
  stepCount: {
    minWidth: "80px",
  },
}));

export const PlaybackStepCounter = (props) => {
  const classes = useStyles();

  return (
    <Tooltip title="Step count">
      <Typography data-id="info-stepCount" className={classes.stepCount} color="primary" align="right">
        {props.stepIndex}/{props.stepCount - 1}
      </Typography>
    </Tooltip>
  );
};