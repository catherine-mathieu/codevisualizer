import {makeStyles} from "@material-ui/core/styles";
import React from "react";

// `VariableBox` displays a variable name and a box for its value

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    alignItems: "center",
    marginTop: theme.spacing(0.5),
  },

  variableName: {
    minWidth: "100px",
    textAlign: "right",
    fontWeight: "bold",
    marginRight: theme.spacing(1),
  },

  variableValue: {
    minWidth: theme.spacing(5),
    textAlign: "center",
    border: "2px solid lightgray",
    padding: "2px 8px",
  },

  variableValueHighlight: {
    minWidth: theme.spacing(5),
    backgroundColor: "yellow",
    textAlign: "center",
    border: "2px solid lightgray",
    padding: "2px 8px",
  },

  variableValueHighlightRed: {
    minWidth: theme.spacing(5),
    backgroundColor: "red",
    textAlign: "center",
    border: "2px solid lightgray",
    padding: "2px 8px",
  },
}));

const getValueText = (value) => {
  switch (value) {
  case null:
    return "null";
  case undefined:
    return "undefined";
  case true:
    return "true";
  case false:
    return "false";
  default:
    if (typeof value === "string") {
      return `"${value}"`;
    } else if (typeof value === "object") {
      return JSON.stringify(value, null, 2);
    } else if (isNaN(value)) {
      return "NaN";
    }
    return value;
  }
};

export const VariableBox = (props) => {
  const classes = useStyles();
  let variableValueStyle = props.highlight
    ? classes.variableValueHighlight
    : classes.variableValue;

  if (props.isVisibleInScope === false) {
    variableValueStyle = classes.variableValueHighlightRed;
  }

  const value = getValueText(props.value);

  return (
    <div data-id={props["data-id"]} className={classes.root}>
      <div className={classes.variableName}>{props.name}</div>
      <div className={variableValueStyle}>{value}</div>
    </div>
  );
};