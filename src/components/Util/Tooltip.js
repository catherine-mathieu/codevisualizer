import React from "react";
import MuiTooltip from "@material-ui/core/Tooltip";

const Tooltip = (props) => {
  const closerProps = props.closer
    ? {PopperProps: {className: "MuiTooltip-popper-button"}, closer: undefined}
    : {};

  props = {...props, ...closerProps};

  return (
    <MuiTooltip placement="top" {...props}>
      {props.children}
    </MuiTooltip>
  );
};

export {Tooltip};