import React from "react";
import ReactDOM from "react-dom";
import {ErrorBoundary} from "../components/Util/ErrorBoundary";
import {ApplicationBar} from "../components/Features/ApplicationBar";
import {ApplicationTheme} from "../components/Features/ApplicationTheme";

const ControllerApplicationBar = () => {
  return (
    <ErrorBoundary>
      <ApplicationTheme>
        <ApplicationBar />
      </ApplicationTheme>
    </ErrorBoundary>
  );
};

ReactDOM.render(<ControllerApplicationBar />, document.getElementById("application"));
