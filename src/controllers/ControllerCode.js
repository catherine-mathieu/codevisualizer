import React from "react";
import ReactDOM from "react-dom";
import {CodeEditor} from "../components/Features/CodeEditor";

// `ControllerCode` handles and renders a code editor with highlightable range

const domContainerCodeEditor = document.getElementById('codeEditor');

export class ControllerCode {
  constructor(onCodeChange){
    this.onCodeChange = onCodeChange;
  }

  setHighlight = (startIndex, endIndex) => {
    this.startIndex = startIndex;
    this.endIndex = endIndex;

    this.render();
  };

  setCode = (code) => {
    this.code = code.trim();

    this.onCodeChange();
  };

  render = () => {
    const codeEditorProps = {
      code: this.code,
      startIndex: this.startIndex,
      endIndex: this.endIndex,
      onCodeChange: this.setCode,
    };

    ReactDOM.render(<CodeEditor {...codeEditorProps}/>, domContainerCodeEditor);
  };
}