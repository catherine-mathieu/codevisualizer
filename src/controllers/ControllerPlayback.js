import React from "react";
import ReactDOM from "react-dom";
import {ApplicationTheme} from "../components/Features/ApplicationTheme";
import {Playback} from "../components/Features/Playback";
import {ErrorBoundary} from "../components/Util/ErrorBoundary";
import {UtilsMath} from "../utils/UtilsMath";

// `ControllerPlayback` handles keyboard event and renders a slider with the total number of steps

const domContainerPlayback = document.getElementById('playback');

export class ControllerPlayback {
  constructor(onStepChange, onCodeBuild, onCodeFragmentSelect, codeFragmentNames) {
    this.stepCount = 0;
    this.stepIndex = 0;
    this.onStepChange = onStepChange;
    this.onCodeBuild = onCodeBuild;
    this.onCodeFragmentSelect = onCodeFragmentSelect;
    this.codeFragmentNames = codeFragmentNames;
    this.registerKeyboardShortcuts();
  }

  reset = (stepCount) => {
    this.stepCount = stepCount;
    this.stepIndex = 0;

    this.render();
  };

  setStepIndex = (stepIndex) => {
    this.stepIndex = UtilsMath.clamp(stepIndex, 0, this.stepCount - 1);

    this.render();
    this.onStepChange(this.stepIndex);
  };

  render = () =>{
    const playbackProps = {
      stepIndex: this.stepIndex,
      stepCount: this.stepCount,
      codeFragmentNames: this.codeFragmentNames,
      onChange: this.setStepIndex,
      onCodeFragmentSelect: this.onCodeFragmentSelect,
      onCodeBuildClick: this.onCodeBuild,
    };

    ReactDOM.render(
      <ErrorBoundary>
        <ApplicationTheme>
            <Playback {...playbackProps}/>
        </ApplicationTheme>
      </ErrorBoundary>,
      domContainerPlayback);
  };

  registerKeyboardShortcuts = () => {
    document.addEventListener("keydown", event => {
      if (event.keyCode === 39) {
        this.setStepIndex(this.stepIndex + 1);
      } else if (event.keyCode === 37) {
        this.setStepIndex(this.stepIndex - 1);
      }
    });
  };
}