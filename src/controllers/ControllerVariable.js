import React from "react";
import ReactDOM from "react-dom";
import {DataVariable} from "../components/Features/DataVariable";

// `ControllerData` handles and renders a representation of the variables

const domContainerDataVariable = document.getElementById('dataVariable');

export class ControllerVariable {
  setVariables = (variables, variableChange, isVisibleInScope) => {
    this.variables = variables;
    this.variableChange = variableChange;
    this.isVisibleInScope = isVisibleInScope;

    this.render();
  };

  render = () => {
    const dataVariableProps = {
      variables: this.variables,
      variableChange: this.variableChange,
      isVisibleInScope: this.isVisibleInScope,
    };

    ReactDOM.render(<DataVariable {...dataVariableProps}/>, domContainerDataVariable);
  };
}