import React from "react";
import ReactDOM from "react-dom";
import {DataVisualization} from "../components/Features/DataVisualization";

// `ControllerData` handles and renders a representation of the variables

const domContainerDataVisualization = document.getElementById('dataVisualization');

export class ControllerVisualization {
  setVariables = (variables, variableChange) => {
    this.variables = variables;
    this.variableChange = variableChange;

    this.render();
  };

  render = () => {
    const dataVisualizationProps = {
      variables: this.variables,
      variableChange: this.variableChange,
    };

    ReactDOM.render(<DataVisualization {...dataVisualizationProps}/>, domContainerDataVisualization);
  };
}