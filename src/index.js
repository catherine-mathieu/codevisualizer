import {CodeAnalyzer} from "./codeAnalysis/CodeAnalyzer";
import {codeFragmentLibrary} from "./codeFragments/codeFragmentLibrary";
import "./controllers/ControllerApplicationBar";
import {ControllerCode} from "./controllers/ControllerCode";
import {ControllerPlayback} from "./controllers/ControllerPlayback";
import {ControllerVariable} from "./controllers/ControllerVariable";
import {ControllerVisualization} from "./controllers/ControllerVisualization";

// `index.js` is the entry point of the application.
// It creates instances of various controllers and handles the application main events

const defaultCode = codeFragmentLibrary[0].code;
const codeFragmentNames = codeFragmentLibrary
  .map(codeFragment => codeFragment.name);

const onStepChange = (index) => {
  const state = codeAnalyzer.executionStates[index];

  codeEditor.setHighlight(state.startIndex, state.endIndex);
  dataVariable.setVariables(state.variables, state.variableChange);
  dataVisualization.setVariables(state.variables, state.variableChange);
};

const onCodeChange = () => {
  codeEditor.setHighlight(0, 0);
  dataVariable.setVariables([], undefined, undefined);
  dataVisualization.setVariables([], undefined);
  playback.reset(1);
};

const onCodeBuild = () => {
  codeAnalyzer.updateExecutionState(codeEditor.code);
  playback.reset(codeAnalyzer.executionStates.length);
  onStepChange(0);
};

const onCodeFragmentSelect = (codeFragmentIndex) => {
  codeEditor.setCode(codeFragmentLibrary[codeFragmentIndex].code);
  setTimeout(() => {
    onCodeBuild();
  }, 0);
};

const codeAnalyzer = new CodeAnalyzer();
const playback = new ControllerPlayback(onStepChange, onCodeBuild, onCodeFragmentSelect, codeFragmentNames);
const codeEditor = new ControllerCode(onCodeChange);
const dataVariable = new ControllerVariable();
const dataVisualization = new ControllerVisualization();

codeEditor.setCode(defaultCode);
onCodeBuild();