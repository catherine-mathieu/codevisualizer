import {featurePrimitiveType} from "../codeFragments/features/featurePrimitiveType";
import {featureScope} from "../codeFragments/features/featureScope";
import {beforeAllAsync, clickDomAsync, getDomTextAsync, timeout} from "../utils/UtilsTestDom";

beforeAll(async () => await beforeAllAsync(require("puppeteer")));

describe("Test DataVariable", () =>
{
  test("Increasing a code fragment step by step displays the correct data variable names.", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featureScope.name}`}, page);

    const valueVariable0 = await getDomTextAsync({cssSelector: "[data-id=\"variable-0-a\"] > :nth-of-type(1)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable1 = await getDomTextAsync({cssSelector: "[data-id=\"variable-1-b\"] > :nth-of-type(1)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable2 = await getDomTextAsync({cssSelector: "[data-id=\"variable-2-c\"] > :nth-of-type(1)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable3 = await getDomTextAsync({cssSelector: "[data-id=\"variable-3-d\"] > :nth-of-type(1)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable4 = await getDomTextAsync({cssSelector: "[data-id=\"variable-4-e\"] > :nth-of-type(1)"}, page);

    expect(valueVariable0).toBe("a");
    expect(valueVariable1).toBe("b");
    expect(valueVariable2).toBe("c");
    expect(valueVariable3).toBe("d");
    expect(valueVariable4).toBe("e");
  }, timeout);

  test("Increasing a code fragment step by step displays the correct data variable values.", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featureScope.name}`}, page);

    const valueVariable0 = await getDomTextAsync({cssSelector: "[data-id=\"variable-0-a\"] > :nth-of-type(2)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable1 = await getDomTextAsync({cssSelector: "[data-id=\"variable-1-b\"] > :nth-of-type(2)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable2 = await getDomTextAsync({cssSelector: "[data-id=\"variable-2-c\"] > :nth-of-type(2)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable3 = await getDomTextAsync({cssSelector: "[data-id=\"variable-3-d\"] > :nth-of-type(2)"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const valueVariable4 = await getDomTextAsync({cssSelector: "[data-id=\"variable-4-e\"] > :nth-of-type(2)"}, page);

    expect(valueVariable0).toBe("\"a\"");
    expect(valueVariable1).toBe("\"b\"");
    expect(valueVariable2).toBe("\"c\"");
    expect(valueVariable3).toBe("\"d\"");
    expect(valueVariable4).toBe("\"e\"");
  }, timeout);

  test("Increasing a Primitive code fragment displays the correct data variable values.", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featurePrimitiveType.name}`}, page);

    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    const valueVariable0 = await getDomTextAsync({cssSelector: "[data-id=\"variable-0-booleanValue\"] > :nth-of-type(2)"}, page);
    const valueVariable1 = await getDomTextAsync({cssSelector: "[data-id=\"variable-1-nullValue\"] > :nth-of-type(2)"}, page);
    const valueVariable2 = await getDomTextAsync({cssSelector: "[data-id=\"variable-2-text\"] > :nth-of-type(2)"}, page);
    const valueVariable3 = await getDomTextAsync({cssSelector: "[data-id=\"variable-3-numberValue\"] > :nth-of-type(2)"}, page);
    const valueVariable4 = await getDomTextAsync({cssSelector: "[data-id=\"variable-4-infinityValue\"] > :nth-of-type(2)"}, page);
    const valueVariable5 = await getDomTextAsync({cssSelector: "[data-id=\"variable-5-notANumberValue\"] > :nth-of-type(2)"}, page);
    const valueVariable6 = await getDomTextAsync({cssSelector: "[data-id=\"variable-6-undefinedValue\"] > :nth-of-type(2)"}, page);

    expect(valueVariable0).toBe("false");
    expect(valueVariable1).toBe("null");
    expect(valueVariable2).toBe("\"This is a string.\"");
    expect(valueVariable3).toBe("10");
    expect(valueVariable4).toBe("Infinity");
    expect(valueVariable5).toBe("NaN");
    expect(valueVariable6).toBe("undefined");
  }, timeout);
});