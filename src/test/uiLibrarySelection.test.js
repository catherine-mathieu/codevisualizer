import {algorithmBubbleSort} from "../codeFragments/algorithms/algorithmBubbleSort";
import {featureForLoop} from "../codeFragments/features/featureForLoop";
import {beforeAllAsync, clickDomAsync, getDomTextAsync, timeout} from "../utils/UtilsTestDom";

beforeAll(async () => await beforeAllAsync(require("puppeteer")));

describe("Test Playback Library select button", () => {
  test("Selecting a code fragment in the library displays the correct text in the code editor", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${algorithmBubbleSort.name}`}, page);
    const codeContent = await getDomTextAsync({dataId: "codeEditorContentRaw"}, page);

    const codeFragmentExpected = algorithmBubbleSort.code.trim();

    expect(codeFragmentExpected).toBe(codeContent.trim());
  }, timeout);

  test("Select another code fragment from the library", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featureForLoop.name}`}, page);
  }, timeout);

  test("Select another code fragment and select again the first code fragment from the library", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featureForLoop.name}`}, page);
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${algorithmBubbleSort.name}`}, page);
  }, timeout);
});