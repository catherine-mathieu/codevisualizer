import {beforeAllAsync, clickDomAsync, getCurrentStepIndexAsync, getStepCount, timeout} from "../utils/UtilsTestDom";

beforeAll(async () => await beforeAllAsync(require("puppeteer")));

describe("Test Playback Controls Play button", () => {
  test("Clicking on Play button and waiting 2 seconds increases the current step by at least 2 steps.", async () => {
    await clickDomAsync({dataId: "button-playbackControlPlay"}, page);

    //todo: Complete later cause it will definitely fail.
  }, timeout);

  test("Clicking on Play button should toggle the button to a Pause button.", async () => {
    await clickDomAsync({dataId: "button-playbackControlPlay"}, page);

    //todo: Complete later cause it will definitely fail.
  }, timeout);
});

describe("Test Playback Controls Next button", () => {
  test("Clicking on Next button increases the current step by exactly one step.", async () => {
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const stepCounterIndexAfterNext = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterNext).toBe(stepCounterIndex + 1);
  }, timeout);

  test("Clicking on Next button twice increases the current step by exactly two steps.", async () => {
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const stepCounterIndexAfterNext = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterNext).toBe(stepCounterIndex + 2);
  }, timeout);

  test("Clicking on Next button once after clicking on LastStep button increases the current step to the last step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const stepCounterIndexAfterNext = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterNext).toBe(stepCounterIndex);
  }, timeout);
});

describe("Test Playback Controls Previous button", () => {
  test("Clicking on Previous button once while in the first step does not change the current step.", async () => {
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlPrevious"}, page);
    const stepCounterIndexAfterPrevious = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterPrevious).toBe(stepCounterIndex);
  }, timeout);

  test("Clicking on Previous button once while in the third step decreases the current step by exactly one step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlPrevious"}, page);
    const stepCounterIndexAfterPrevious = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterPrevious).toBe(stepCounterIndex - 1);
  }, timeout);

  test("Clicking on Previous button after clicking on FirstStep button does not change the current step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlPrevious"}, page);
    const stepCounterIndexAfterPrevious = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterPrevious).toBe(stepCounterIndex);
  }, timeout);
});

describe("Test Playback Controls FirstStep button", () => {
  test("Clicking on FirstStep button once while in the first step does not change the current step.", async () => {
    const stepCounterIndex = await getCurrentStepIndexAsync(page);
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    const stepCounterIndexAfterFirstStep = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterFirstStep).toBe(stepCounterIndex);
  }, timeout);

  test("Clicking on FirstStep button once while in the third step decreases the current step to the first step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    const stepCounterIndexAfterFirstStep = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterFirstStep).toBe(1);
  }, timeout);

  test("Clicking on FirstStep button once while in the last step decreases the current step to the first step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    const stepCounterIndexAfterFirstStep = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterFirstStep).toBe(1);
  }, timeout);

  test("Clicking on FirstStep button twice while in the last step decreases the current step to the first step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    await clickDomAsync({dataId: "button-playbackControlFirstStep"}, page);
    const stepCounterIndexAfterFirstStep = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexAfterFirstStep).toBe(1);
  }, timeout);
});

describe("Test Playback Controls LastStep button", () => {
  test("Clicking on LastStep button once while in the first step increases the current step to the last step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    const stepCountAfterLastStep = await getCurrentStepIndexAsync(page);
    const stepCount = await getStepCount(page);

    expect(stepCountAfterLastStep).toBe(stepCount);
  }, timeout);

  test("Clicking on LastStep button once while in the third step increases the current step to the last step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlNext"}, page);
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    const stepCountAfterLastStep = await getCurrentStepIndexAsync(page);
    const stepCount = await getStepCount(page);

    expect(stepCountAfterLastStep).toBe(stepCount);
  }, timeout);

  test("Clicking on LastStep button twice increases the current step to the last step.", async () => {
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    await clickDomAsync({dataId: "button-playbackControlLastStep"}, page);
    const stepCountAfterLastStep = await getCurrentStepIndexAsync(page);
    const stepCount = await getStepCount(page);

    expect(stepCountAfterLastStep).toBe(stepCount);
  }, timeout);
});