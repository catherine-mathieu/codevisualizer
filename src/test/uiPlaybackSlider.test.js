import {featureScope} from "../codeFragments/features/featureScope";
import {
  beforeAllAsync,
  clickDomAsync,
  getCurrentStepIndexAsync,
  getStepCount,
  slideDomAsync,
  timeout
} from "../utils/UtilsTestDom";

beforeAll(async () => await beforeAllAsync(require("puppeteer")));

describe("Test Playback Step Progression Slider", () => {
  test("Moving the StepProgression slider by exactly one step increases the current step by exactly one step.", async () => {
    await clickDomAsync({cssSelector: ".PlaybackLibrary"}, page);
    await clickDomAsync({dataId: `MenuItem-${featureScope.name}`}, page);
    const stepCount = await getStepCount(page);
    await slideDomAsync(
      {dataId: "slider-playbackControlStepProgression"},
      {cssSelector: ".MuiSlider-thumb"},
      stepCount,
      page
    );
    const stepCounterIndexSliding = await getCurrentStepIndexAsync(page);

    expect(stepCounterIndexSliding).toBe(1);
  }, timeout);
});