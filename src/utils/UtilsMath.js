export class UtilsMath {
  static clamp(value, minimum, maximum) {
    return Math.min(Math.max(value, minimum), maximum);
  }
}