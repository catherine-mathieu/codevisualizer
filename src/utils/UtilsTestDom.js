export const timeout = process.env.SLOWMO ? 60000 : 50000;

// Todo: use JEST Environment : https://stackoverflow.com/questions/47997652/jest-beforeall-share-between-multiple-test-files

export const beforeAllAsync = async () => {
  await page.goto("http://localhost:9000", {waitUntil: "domcontentloaded"});
};

export const getDomAsync = async (searchOptions, page) => {
  if (searchOptions.cssSelector !== undefined) {
    return await page.$(searchOptions.cssSelector);
  } else if (searchOptions.dataId !== undefined) {
    return await page.$(`[data-id="${searchOptions.dataId}"]`);
  }

  return undefined;
};

export const getDomTextAsync = async (searchOptions, page) => {
  const dom = await getDomAsync(searchOptions, page);

  return await page.evaluate(element => element.textContent, dom);
};

export const waitIfDebugAsync = async () => {
  await new Promise(resolve => setTimeout(resolve, 500));
};

export const clickDomAsync = async (searchOptions, page) => {
  const dom = await getDomAsync(searchOptions, page);
  await dom.click();
  await waitIfDebugAsync();

  return dom;
};

export const slideDomAsync = async (sliderSearchOptions, handleSliderSearchOptions, stepCount, page) => {
  const sliderElement = await getDomAsync(sliderSearchOptions, page);
  const slider = await sliderElement.boundingBox();

  const sliderHandle = await getDomAsync(handleSliderSearchOptions, page);
  const handle = await sliderHandle.boundingBox();

  await page.mouse.move(
    handle.x + handle.width / 2,
    handle.y + handle.height / 2
  );
  await page.mouse.down();
  await page.mouse.move(handle.x + slider.width / stepCount, handle.y + handle.height / 2, {
    steps: 1
  });
  await page.mouse.up();
  await waitIfDebugAsync();

  return handle;
};

export const getCurrentStepIndexAsync = async (page) => {
  const stepCount = await getDomTextAsync({dataId: "info-stepCount"}, page);
  const stepCountIndex = stepCount.substring(0, stepCount.indexOf("/"));

  return parseInt(stepCountIndex, 10);
};

export const getStepCount = async (page) => {
  const stepCount = await getDomTextAsync({dataId: "info-stepCount"}, page);
  const stepCountTotal = stepCount.substring(stepCount.indexOf("/") + 1);

  return parseInt(stepCountTotal, 10);
};