import {UtilsMath} from "./UtilsMath";

describe("Utils Math tests", () => {
  test('Value is clamp to its value if does not exceed the maximum allowed or smaller than the minimum.', () => {
    expect(UtilsMath.clamp(5, 0, 10)).toBe(5);
  });

  test('Value is clamp to the minimum value allowed if the value is smaller.', () => {
    expect(UtilsMath.clamp(-1, 0, 10)).toBe(0);
  });

  test('Value is clamp to the maximum value allowed if the value is greater.', () => {
    expect(UtilsMath.clamp(100, 0, 10)).toBe(10);
  });

  test('Value is not clamp and return not a number if it is undefined.', () => {
    expect(UtilsMath.clamp(undefined, 0, 10)).toBe(NaN);
  });

  test('Value is not clamp and return not a number if it is an array.', () => {
    expect(UtilsMath.clamp([1, 3, 15, -1], 0, 10)).toBe(NaN);
  });
});